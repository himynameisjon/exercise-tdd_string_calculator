<?php
namespace StringCalculator;

interface CalculatorInterface
{
    /**
     * @param string $values - Delimited list of integers
     * @returns int - Sum of integers in $values
     * @throws \InvalidArgumentException
     */
    public function add($values);
}
