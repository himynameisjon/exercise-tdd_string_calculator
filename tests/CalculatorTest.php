<?php namespace StringCalculator;

class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testCalculatorExists() {
        $this->assertInstanceOf(Calculator::class, new Calculator());
    }
}
