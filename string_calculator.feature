Feature: Calculate the sum of values in a string
    We need a function to calculate the sum of string of integers
    String can be comma- or newline-delimited
    Negative numbers are not supported
    Numbers greater than 1000 must be ignored

    Scenario: Calculate empty strings
		When The calculation string is empty
        Then The result should be 0

    Scenario: Calculate a single value
		When The calculation string contains a single value
        Then The result should be equal to that value

    Scenario: Calculate numbers delimited by commas
		When The calculation string contains multiple values
        And The string is delimited by commas
        Then The result should be equal to the sum of all values

    Scenario: Calculate numbers delimited by newlines
		When The calculation string contains multiple integers
        And The string is delimited by newlines
        Then The result should be equal to the sum of all values

    Scenario: Specify delimiter on first line
        Given A multi-line calculation string is provided
		When The first line is prefixed with double forward slash (e.g. //)
        Then The character following the slashes must be used as the delimiter
