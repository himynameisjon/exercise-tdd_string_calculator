# String Calculator Exercise

## Prerequisites
- PHP 5.6+
- Composer

## Install Dependencies
1. Run `composer install`

## Run Tests
1. Run `./vendor/bin/phpunit`
